(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["agendados-servicios-adicionales-servicios-adicionales-module"],{

/***/ "0y9n":
/*!**************************************************************************************************!*\
  !*** ./src/app/profesional/home/agendados/servicios-adicionales/servicios-adicionales.page.scss ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".no-border {\n  padding-top: 2px;\n  padding-bottom: 2px;\n}\n\n.profileImg {\n  margin-left: 10px;\n  height: 75px;\n  width: 75px;\n}\n\n.titleSelect {\n  font-size: 23px;\n  font-weight: bold;\n}\n\n.tittle {\n  font-size: 18px;\n}\n\n.border {\n  border: 1px solid #009ACE;\n}\n\n.regText {\n  font-size: 14px;\n}\n\n.imgSingle {\n  height: 125px;\n}\n\n.locate-cont {\n  border-radius: 50px;\n  height: 50px;\n  width: 50px;\n  display: inline-flex;\n  align-items: center;\n  text-align: center;\n  margin-left: auto;\n  margin-right: auto;\n  background-color: #009ACE;\n}\n\n.rating-text {\n  font-size: 26px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.ticket {\n  color: red;\n  font-size: 12px;\n}\n\n.textSelect {\n  font-size: 14px;\n}\n\n.minusMargin {\n  margin-top: -20px;\n}\n\n.subMiniTex {\n  font-size: 16px;\n}\n\n.addIcon {\n  font-size: 35px;\n}\n\n.border-right {\n  border-right: 1px solid red;\n}\n\n.redText {\n  color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NlcnZpY2lvcy1hZGljaW9uYWxlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBQ0E7RUFFSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBQ0E7RUFFSSxlQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFDQTtFQUVJLGVBQUE7QUFDSjs7QUFDQTtFQUVJLHlCQUFBO0FBQ0o7O0FBQ0E7RUFFSSxlQUFBO0FBQ0o7O0FBQ0E7RUFFSSxhQUFBO0FBQ0o7O0FBQ0E7RUFFSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBQ0E7RUFFQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNBOztBQUNBO0VBRUksVUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFDQTtFQUVJLGVBQUE7QUFDSjs7QUFDQTtFQUVJLGlCQUFBO0FBQ0o7O0FBQ0E7RUFFSSxlQUFBO0FBQ0o7O0FBQ0E7RUFFSSxlQUFBO0FBQ0o7O0FBQ0E7RUFFSSwyQkFBQTtBQUNKOztBQUNBO0VBRUksVUFBQTtBQUNKIiwiZmlsZSI6InNlcnZpY2lvcy1hZGljaW9uYWxlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubm8tYm9yZGVye1xuICAgIHBhZGRpbmctdG9wOiAycHg7XG4gICAgcGFkZGluZy1ib3R0b206IDJweDtcbn1cbi5wcm9maWxlSW1nXG57XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgaGVpZ2h0OiA3NXB4O1xuICAgIHdpZHRoOiA3NXB4O1xufVxuLnRpdGxlU2VsZWN0XG57XG4gICAgZm9udC1zaXplOiAyM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnRpdHRsZVxue1xuICAgIGZvbnQtc2l6ZTogMThweDtcbn1cbi5ib3JkZXJcbntcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDA5QUNFO1xufVxuLnJlZ1RleHRcbntcbiAgICBmb250LXNpemU6IDE0cHg7XG59XG4uaW1nU2luZ2xlXG57XG4gICAgaGVpZ2h0OiAxMjVweDtcbn1cbi5sb2NhdGUtY29udFxue1xuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIHdpZHRoOiA1MHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA5QUNFO1xufVxuLnJhdGluZy10ZXh0XG57XG5mb250LXNpemU6IDI2cHg7XG5tYXJnaW4tbGVmdDogYXV0bztcbm1hcmdpbi1yaWdodDogYXV0bztcbn1cbi50aWNrZXRcbntcbiAgICBjb2xvcjogcmVkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cbi50ZXh0U2VsZWN0XG57XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuLm1pbnVzTWFyZ2luXG57XG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XG59XG4uc3ViTWluaVRleFxue1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbn1cbi5hZGRJY29uXG57XG4gICAgZm9udC1zaXplOiAzNXB4O1xufVxuLmJvcmRlci1yaWdodFxue1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHJlZDtcbn1cbi5yZWRUZXh0XG57XG4gICAgY29sb3I6IHJlZDtcbn1cbiAgIl19 */");

/***/ }),

/***/ "39PD":
/*!**********************************************************************************************************!*\
  !*** ./src/app/profesional/home/agendados/servicios-adicionales/servicios-adicionales-routing.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: ServiciosAdicionalesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosAdicionalesPageRoutingModule", function() { return ServiciosAdicionalesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _servicios_adicionales_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./servicios-adicionales.page */ "Bhnx");




const routes = [
    {
        path: '',
        component: _servicios_adicionales_page__WEBPACK_IMPORTED_MODULE_3__["ServiciosAdicionalesPage"]
    }
];
let ServiciosAdicionalesPageRoutingModule = class ServiciosAdicionalesPageRoutingModule {
};
ServiciosAdicionalesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ServiciosAdicionalesPageRoutingModule);



/***/ }),

/***/ "Bhnx":
/*!************************************************************************************************!*\
  !*** ./src/app/profesional/home/agendados/servicios-adicionales/servicios-adicionales.page.ts ***!
  \************************************************************************************************/
/*! exports provided: ServiciosAdicionalesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosAdicionalesPage", function() { return ServiciosAdicionalesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_servicios_adicionales_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./servicios-adicionales.page.html */ "CI2/");
/* harmony import */ var _servicios_adicionales_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./servicios-adicionales.page.scss */ "0y9n");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");






let ServiciosAdicionalesPage = class ServiciosAdicionalesPage {
    constructor(router, menuController) {
        this.router = router;
        this.menuController = menuController;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.menuController.enable(true, 'profesional');
    }
    openMenu() {
        this.menuController.open();
    }
    saveExtra() {
        this.router.navigate(['/profesional/home/home-tabs/agendados/agendados-finalizar']);
    }
};
ServiciosAdicionalesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] }
];
ServiciosAdicionalesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-servicios-adicionales',
        template: _raw_loader_servicios_adicionales_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_servicios_adicionales_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ServiciosAdicionalesPage);



/***/ }),

/***/ "CI2/":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profesional/home/agendados/servicios-adicionales/servicios-adicionales.page.html ***!
  \****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    \n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/profesional/home/home-tabs/agendados/agendados-finalizar\" text=\"\" icon=\"arrow-back\"></ion-back-button>\n      <ion-button class=\"homeBtn\" (click)=\"openMenu()\">\n        <ion-icon name=\"menu\" class=\"homeBtn\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n    <ion-title class=\"title-toolbar\">SERVICIOS ADICIONALES</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <!-- profesional card item -->\n  <div class=\"no-border\">\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n        <!-- profile img -->\n        <ion-col size=\"4\" offset=\"1\">\n          <ion-avatar class=\"profileImg\">\n            <img src=\"/assets/images/avatar.png\"/>\n          </ion-avatar>\n        </ion-col>\n\n        <!-- title -->\n        <ion-col size=\"7\">\n          <ion-text>\n            <small class=\"ticket\">Ticket #100091234</small>\n            <span class=\"titleSelect main-color\">\n              Cerrajería para<br>\n              Emmy Mut\n            </span>\n            <br>\n            <p class=\"textSelect main-color\" style=\"margin-top: 0; margin-bottom: 0;\">27 nov 2020</p>\n          </ion-text>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <ion-grid>\n\n    <!-- text -->\n    <ion-row>\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"10\" class=\"ion-text-center\">\n        <ion-text class=\"main-color regText\"><b>Detalles del trabajo adicional</b></ion-text>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n    </ion-row>\n\n    <!-- text-area -->\n    <ion-row class=\"ion-margin-top\">\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"10\" class=\"border ion-text-center\">\n        <ion-item lines=\"none\">\n           <ion-textarea rows=\"3\" cols=\"20\" placeholder=\"Describe los trabajos adicionales. Máximo 250 caracteres con espacios.\"></ion-textarea>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n    </ion-row>\n\n    <!-- title -->\n    <ion-row class=\"ion-margin-top\">\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"10\" class=\"ion-text-center\">\n        <ion-text class=\"main-color title\"><b>Fotografías del trabajo realizado</b></ion-text>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n    </ion-row>\n\n    <!-- images -->\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"4\" offset=\"1\">\n        <img src=\"/assets/images/unavailable-image.png\" class=\"imgSingle\">\n      </ion-col>\n      <ion-col size=\"4\">\n        <img src=\"/assets/images/unavailable-image.png\" class=\"imgSingle\">\n      </ion-col>\n      <ion-col size=\"3\">\n        <div class=\"locate-cont\">\n          <ion-icon name=\"add\" color=\"light\" class=\"rating-text\"></ion-icon>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <!-- title -->\n    <ion-row>\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"10\" class=\"ion-text-center\">\n        <ion-text class=\"main-color title\"><b>Captura el costo <span class=\"redText\">*</span></b></ion-text>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n    </ion-row>\n\n    <!-- email -->\n    <ion-row>\n      <ion-col size=\"11\">\n        <ion-item>\n          <ion-label position=\"floating\">\n            <ion-icon name=\"wallet\" slot=\"start\" class=\"main-color\"></ion-icon>\n            <ion-text class=\"main-color\">&nbsp;&nbsp;&nbsp;Ingresa el costo del servicio.</ion-text>\n          </ion-label>\n          <ion-input type=\"text\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n     <!-- GUARDAR trabajo BTN -->\n     <ion-row class=\"ion-margin-top ion-margin-bottom\">\n      <ion-col size=\"1\"></ion-col>\n      <ion-col>\n        <ion-button size=\"5\" expand=\"block\" class=\"ion-text-uppercase\" color=\"primary\" (click)=\"saveExtra()\">\n          GUARDAR\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n");

/***/ }),

/***/ "mZJI":
/*!**************************************************************************************************!*\
  !*** ./src/app/profesional/home/agendados/servicios-adicionales/servicios-adicionales.module.ts ***!
  \**************************************************************************************************/
/*! exports provided: ServiciosAdicionalesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosAdicionalesPageModule", function() { return ServiciosAdicionalesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _servicios_adicionales_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./servicios-adicionales-routing.module */ "39PD");
/* harmony import */ var _servicios_adicionales_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./servicios-adicionales.page */ "Bhnx");







let ServiciosAdicionalesPageModule = class ServiciosAdicionalesPageModule {
};
ServiciosAdicionalesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _servicios_adicionales_routing_module__WEBPACK_IMPORTED_MODULE_5__["ServiciosAdicionalesPageRoutingModule"]
        ],
        declarations: [_servicios_adicionales_page__WEBPACK_IMPORTED_MODULE_6__["ServiciosAdicionalesPage"]]
    })
], ServiciosAdicionalesPageModule);



/***/ })

}]);
//# sourceMappingURL=agendados-servicios-adicionales-servicios-adicionales-module-es2015.js.map