(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profesional-rating-rating-module"],{

/***/ "3+3v":
/*!***************************************************!*\
  !*** ./src/app/profesional/rating/rating.page.ts ***!
  \***************************************************/
/*! exports provided: RatingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingPage", function() { return RatingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_rating_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./rating.page.html */ "668C");
/* harmony import */ var _rating_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./rating.page.scss */ "kzeB");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





let RatingPage = class RatingPage {
    constructor(menuController) {
        this.menuController = menuController;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.menuController.enable(true, 'profesional');
    }
    openMenu() {
        this.menuController.open();
    }
};
RatingPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] }
];
RatingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-rating',
        template: _raw_loader_rating_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_rating_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RatingPage);



/***/ }),

/***/ "668C":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profesional/rating/rating.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/profesional/home\" text=\"\" icon=\"arrow-back\"></ion-back-button>\n      <ion-button class=\"homeBtn\" (click)=\"openMenu()\">\n        <ion-icon name=\"menu\" class=\"homeBtn\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n    <ion-title class=\"title-toolbar\">CALIFICACIONES</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- cat selection -->\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"9\" offset=\"1\">\n        <ion-item>\n          <ion-label class=\"catText\">CATEGORÍAS</ion-label>\n          <ion-select interface=\"popover\">\n            <ion-select-option value=\"nes\" class=\"main-color ion-text-center\">Cerrajería</ion-select-option>\n            <ion-select-option value=\"nes\" class=\"main-color ion-text-center\">Carpinteria</ion-select-option>\n            <ion-select-option value=\"nes\" class=\"main-color ion-text-center\">Electricista</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!-- profesional card item -->\n  <div class=\"no-border\">\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n        <!-- profile img -->\n        <ion-col size=\"4\" offset=\"1\">\n          <ion-avatar class=\"profileImg\">\n            <img src=\"/assets/images/avatar.png\"/>\n          </ion-avatar>\n        </ion-col>\n\n        <!-- title -->\n        <ion-col size=\"6\">\n          <ion-text>\n            <small class=\"ticket\">Ticket #100091234</small><br>\n            <span class=\"titleSelect main-color\">Cerrajería para <br> Juan Alcayaga</span><br>\n            <p class=\"main-color mini-text\" style=\"margin-top: 0; margin-bottom: 0;\">18 nov 2020</p>\n          </ion-text>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <!-- rating / reconocimientos -->\n  <ion-grid fixed>\n    <ion-row>\n      <!-- rating  -->\n      <ion-col size=\"5\" offset=\"1\" class=\"border-line ion-text-center\">\n        <ion-text color=\"primary\" class=\"ratingText\">\n          4.5 / 39 \n          <ion-icon color=\"primary\" name=\"thumbs-up\" class=\"iconFix\"></ion-icon>\n        </ion-text>\n        <br>\n        <ion-icon name=\"star\" color=\"primary\" class=\"starIcon ion-margin-top\"></ion-icon>\n        <ion-icon name=\"star\" color=\"primary\" class=\"starIcon\"></ion-icon>\n        <ion-icon name=\"star\" color=\"primary\" class=\"starIcon\"></ion-icon>\n        <ion-icon name=\"star\" color=\"primary\" class=\"starIcon\"></ion-icon>\n        <ion-icon name=\"star-outline\" color=\"primary\" class=\"starIcon\"></ion-icon>\n      </ion-col>\n      <!-- reconocimiento  -->\n      <ion-col size=\"5\" class=\"ion-text-center\">\n        <ion-text color=\"primary\" class=\"ratingText\">Reconocimientos</ion-text>\n        <br>\n        <!-- icons -->\n        <div class=\"locate-cont ion-margin-top\">\n          <ion-icon name=\"hammer\" color=\"light\" class=\"rating-text\"></ion-icon>\n        </div>\n        <div class=\"locate-cont\">\n          <ion-icon name=\"time\" color=\"light\" class=\"rating-text\"></ion-icon>\n        </div>\n        <div class=\"locate-cont\">\n          <ion-icon name=\"heart\" color=\"light\" class=\"rating-text\"></ion-icon>\n        </div>\n        <div class=\"locate-cont\">\n          <ion-icon name=\"trophy\" color=\"light\" class=\"rating-text\"></ion-icon>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!-- title -->\n  <ion-row class=\"ion-margin-top\">\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"10\" class=\"ion-text-center\">\n      <ion-text class=\"main-color title\"><b>COMENTARIOS</b></ion-text>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n  </ion-row>\n\n  <!-- notificacion item -->\n  <div class=\"prof-cont ion-margin-top\">\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n\n        <!-- title -->\n        <ion-col size=\"8\" class=\"ion-justify-content-center\">\n          <ion-text>\n            <span class=\"titleSelect main-color\">Juan Pérez</span><br>\n            <span class=\"textSelect\">\n              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.\n            </span>\n          </ion-text>\n        </ion-col>\n        <ion-col size=\"1\"></ion-col>\n\n        <!-- rating -->\n        <ion-col size=\"3\" class=\"ion-text-center\">\n          <ion-text>\n            <small class=\"dateText main-color ion-text-center\">\n              16 dic 2020\n            </small>\n          </ion-text>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <!-- notificacion item -->\n  <div class=\"prof-cont ion-margin-top\">\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n\n        <!-- title -->\n        <ion-col size=\"8\" class=\"ion-justify-content-center\">\n          <ion-text>\n            <span class=\"titleSelect main-color\">María Rodríguez</span><br>\n            <span class=\"textSelect\">\n              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.\n            </span>\n          </ion-text>\n        </ion-col>\n        <ion-col size=\"1\"></ion-col>\n\n        <!-- rating -->\n        <ion-col size=\"3\" class=\"ion-text-center\">\n          <ion-text>\n            <small class=\"dateText main-color ion-text-center\">\n              16 dic 2020\n            </small>\n          </ion-text>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <!-- notificacion item -->\n  <div class=\"prof-cont ion-margin-top\">\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n\n        <!-- title -->\n        <ion-col size=\"8\" class=\"ion-justify-content-center\">\n          <ion-text>\n            <span class=\"titleSelect main-color\">Carlos Zepeda</span><br>\n            <span class=\"textSelect\">\n              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.\n            </span>\n          </ion-text>\n        </ion-col>\n        <ion-col size=\"1\"></ion-col>\n\n        <!-- rating -->\n        <ion-col size=\"3\" class=\"ion-text-center\">\n          <ion-text>\n            <small class=\"dateText main-color ion-text-center\">\n              16 dic 2020\n            </small>\n          </ion-text>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <!-- notificacion item -->\n  <div class=\"prof-cont ion-margin-top\">\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n\n        <!-- title -->\n        <ion-col size=\"8\" class=\"ion-justify-content-center\">\n          <ion-text>\n            <span class=\"titleSelect main-color\">Laura Suárez</span><br>\n            <span class=\"textSelect\">\n              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.\n            </span>\n          </ion-text>\n        </ion-col>\n        <ion-col size=\"1\"></ion-col>\n\n        <!-- rating -->\n        <ion-col size=\"3\" class=\"ion-text-center\">\n          <ion-text>\n            <small class=\"dateText main-color ion-text-center\">\n              16 dic 2020\n            </small>\n          </ion-text>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "Tid7":
/*!*************************************************************!*\
  !*** ./src/app/profesional/rating/rating-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: RatingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingPageRoutingModule", function() { return RatingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _rating_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./rating.page */ "3+3v");




const routes = [
    {
        path: '',
        component: _rating_page__WEBPACK_IMPORTED_MODULE_3__["RatingPage"]
    }
];
let RatingPageRoutingModule = class RatingPageRoutingModule {
};
RatingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RatingPageRoutingModule);



/***/ }),

/***/ "hscP":
/*!*****************************************************!*\
  !*** ./src/app/profesional/rating/rating.module.ts ***!
  \*****************************************************/
/*! exports provided: RatingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingPageModule", function() { return RatingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _rating_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rating-routing.module */ "Tid7");
/* harmony import */ var _rating_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rating.page */ "3+3v");







let RatingPageModule = class RatingPageModule {
};
RatingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _rating_routing_module__WEBPACK_IMPORTED_MODULE_5__["RatingPageRoutingModule"]
        ],
        declarations: [_rating_page__WEBPACK_IMPORTED_MODULE_6__["RatingPage"]]
    })
], RatingPageModule);



/***/ }),

/***/ "kzeB":
/*!*****************************************************!*\
  !*** ./src/app/profesional/rating/rating.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".catText {\n  font-size: 10px;\n}\n\n.no-border {\n  padding-top: 2px;\n  padding-bottom: 2px;\n}\n\n.profileImg {\n  margin-left: 10px;\n  height: 75px;\n  width: 75px;\n}\n\n.ticket {\n  color: red;\n  font-size: 12px;\n}\n\n.titleSelect {\n  font-size: 19px;\n  font-weight: bold;\n}\n\n.mini-text {\n  font-size: 14px;\n}\n\n.iconFix {\n  margin-bottom: -2px;\n}\n\n.ratingText {\n  font-size: 18px;\n}\n\n.border-line {\n  border-right: 1px solid red;\n}\n\n.starIcon {\n  font-size: 22px;\n}\n\n.locate-cont {\n  border-radius: 50px;\n  height: 25px;\n  width: 25px;\n  display: inline-flex;\n  align-items: center;\n  text-align: center;\n  margin-left: auto;\n  margin-right: auto;\n  background-color: #009ACE;\n}\n\n.rating-text {\n  font-size: 14px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.title {\n  font-size: 20px;\n  border-bottom: 1px solid red;\n}\n\n.prof-cont {\n  box-shadow: 1px 8px 12px 0px rgba(0, 0, 0, 0.2);\n}\n\n.dateText {\n  font-size: 12px;\n}\n\n.textSelect {\n  font-size: 12px;\n  line-height: 0.5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3JhdGluZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxlQUFBO0FBQUo7O0FBRUE7RUFFSSxnQkFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBRUE7RUFFSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBQUo7O0FBRUE7RUFFSSxVQUFBO0VBQ0EsZUFBQTtBQUFKOztBQUVBO0VBRUksZUFBQTtFQUNBLGlCQUFBO0FBQUo7O0FBRUE7RUFFSSxlQUFBO0FBQUo7O0FBRUE7RUFFSSxtQkFBQTtBQUFKOztBQUVBO0VBRUksZUFBQTtBQUFKOztBQUVBO0VBRUksMkJBQUE7QUFBSjs7QUFFQTtFQUVJLGVBQUE7QUFBSjs7QUFFQTtFQUVJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUFBSjs7QUFFQTtFQUVJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBQUo7O0FBRUE7RUFFSSxlQUFBO0VBQ0EsNEJBQUE7QUFBSjs7QUFFQTtFQUVJLCtDQUFBO0FBQUo7O0FBRUE7RUFFSSxlQUFBO0FBQUo7O0FBRUE7RUFFSSxlQUFBO0VBQ0EsZ0JBQUE7QUFBSiIsImZpbGUiOiJyYXRpbmcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhdFRleHRcbntcbiAgICBmb250LXNpemU6IDEwcHg7XG59XG4ubm8tYm9yZGVyXG57XG4gICAgcGFkZGluZy10b3A6IDJweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMnB4O1xufVxuLnByb2ZpbGVJbWdcbntcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICBoZWlnaHQ6IDc1cHg7XG4gICAgd2lkdGg6IDc1cHg7XG59XG4udGlja2V0XG57XG4gICAgY29sb3I6IHJlZDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG4udGl0bGVTZWxlY3RcbntcbiAgICBmb250LXNpemU6IDE5cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4ubWluaS10ZXh0XG57XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuLmljb25GaXhcbntcbiAgICBtYXJnaW4tYm90dG9tOiAtMnB4O1xufVxuLnJhdGluZ1RleHRcbntcbiAgICBmb250LXNpemU6IDE4cHg7XG59XG4uYm9yZGVyLWxpbmVcbntcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCByZWQ7XG59XG4uc3Rhckljb25cbntcbiAgICBmb250LXNpemU6IDIycHg7XG59XG4ubG9jYXRlLWNvbnRcbntcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgIGhlaWdodDogMjVweDtcbiAgICB3aWR0aDogMjVweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwOUFDRTtcbn1cbi5yYXRpbmctdGV4dFxue1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG4udGl0bGVcbntcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJlZDtcbn1cbi5wcm9mLWNvbnRcbntcbiAgICBib3gtc2hhZG93OiAxcHggOHB4IDEycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4yMCk7XG59XG4uZGF0ZVRleHRcbntcbiAgICBmb250LXNpemU6IDEycHg7XG59XG4udGV4dFNlbGVjdFxue1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogLjU7XG59Il19 */");

/***/ })

}]);
//# sourceMappingURL=profesional-rating-rating-module-es2015.js.map