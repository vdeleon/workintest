export class ProSolicitud{
    constructor(
        clientLastName: string,
        clientName: string,
        date_required: string,
        description: string,
        hours: string,
        id: number,
        name: string,
        status_id: number,
        supplierLastName: string,
        supplierName: string,
        ticket_number: number,
    ){}
}